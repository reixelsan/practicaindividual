package edu.uclm.esi.practicaIndividual.dominio;

import java.sql.Connection;
import java.sql.SQLException;

import edu.uclm.esi.practicaIndividual.persistencia.DAOUsuario;


public class Usuario {

	private String email;
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String pwd1;
	private String telefono;
	private int idUbicacion;
	private int id;
	private Connection bd;
	

	public Usuario(String email, String nombre, String apellido1, String apellido2, String telefono, String pwd1, int ubicacion) {
		this.email=email;
		this.nombre=nombre;
		this.apellido1=apellido1;
		this.apellido2=apellido2;
		this.pwd1=pwd1;
		this.telefono=telefono;
		this.idUbicacion=ubicacion;
	}

	public Usuario(String email, String pwd) throws SQLException, Exception {
		DAOUsuario.identificar(this, email, pwd);
	}

	public String getEmail() {
		return email;
	}


	public String getNombre() {
		return nombre;
	}


	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}



	public String getPwd() {
		return pwd1;
	}



	public String getTelefono() {
		return telefono;
	}



	public int getIdUbicacion() {
		return idUbicacion;
	}


	public void insert() throws Exception {
		DAOUsuario.insert(this);
		
	}

	public void setId(int id) {
		this.id=id;
		
	}

	public void setConnection(Connection bdUsuario) {
		this.bd=bdUsuario;
		
	}


}
